﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DialogTutorTest01
{
    public class CusotmListAdapter : BaseAdapter<Post>
    {
       private Activity context;
       public List<Post> list;

        public CusotmListAdapter(Activity _context, List<Post> _list)
            
        {
            this.context = _context;
            this.list = _list;
        }

        public override Post this[int index]
        {
            get { return list[index]; }
        }


        public override int Count
        {
            get { return list.Count; }
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view = convertView;
            if (view == null)
                view = LayoutInflater.From(context).Inflate(Resource.Layout.listItemRow, null, false);
            Post item = this[position];
            view.FindViewById<TextView>(Resource.Id.textView1).Text = item.title;
            view.FindViewById<TextView>(Resource.Id.textView2).Text = item.description;
            view.FindViewById<ImageView>(Resource.Id.imageView1).SetImageResource(item.image);


            return view;


        }
    }
}
﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DialogTutorTest01
{
    class ListViewFragment : DialogFragment
    {
        private ListView lv;
        private List<Post> mlist;
        CusotmListAdapter adapter;


        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View v = inflater.Inflate(Resource.Layout.fragmentLayout, container, false);
            
            this.Dialog.SetTitle("List of Objects");

            lv = v.FindViewById<ListView>(Resource.Id.lv2);

            List<Post> listData = new List<Post>() {new Post(Resource.Drawable.credit_card,"Test1", "Discription1"),
           new Post(Resource.Drawable.credit_card,"Test2", "Discription2") };

            
            mlist = new List<Post>();
            mlist = listData;
            adapter = new CusotmListAdapter(this.Activity, mlist);
            lv.Adapter = adapter;
            lv.ItemClick += OnListItemClick;


            return v;
        }

        private void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            var select = mlist[e.Position].title;
            Android.Widget.Toast.MakeText(this.Activity, select, Android.Widget.ToastLength.Long).Show();
        }
    }
}